FROM nvidia/cuda:9.0-base-ubuntu16.04

LABEL maintainer="klumblr <klumblr@gmail.com>"

# Pick up some TF dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
        build-essential \
        cuda-command-line-tools-9-0 \
        cuda-cublas-9-0 \
        cuda-cufft-9-0 \
        cuda-curand-9-0 \
        cuda-cusolver-9-0 \
        cuda-cusparse-9-0 \
        curl \
        wget \
        nano \
        unzip \
        zip \
        git \
        locales \
        fonts-liberation \
        libcudnn7=7.0.5.15-1+cuda9.0 \
        libfreetype6-dev \
        libpng12-dev \
        libzmq3-dev \
        pkg-config \
        python \
        python-dev \
        python-setuptools \
        python3 \
        python3-dev \
        python3-setuptools \
        rsync \
        software-properties-common \
        libsm6 \
        libxext6 \
        libopencv-dev \
        libav-tools  \
        libjpeg-dev \
        libpng-dev \
        libtiff-dev \
        libjasper-dev \
        language-pack-ko \
        fonts-nanum-coding \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* 

# Set up locale
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen
    
ENV LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8

# Get pip
RUN curl -O https://bootstrap.pypa.io/get-pip.py && \
    python3 get-pip.py && \
    python get-pip.py && \
    rm get-pip.py
    
RUN pip3 install --upgrade pip && \
    pip3 install \
        Pillow \
        h5py \
        ipykernel \
        jupyter \
        matplotlib \
        numpy \
        pandas \
        scipy \
        sklearn \
        scikit-image \
        opencv-python\
        tqdm \
        glances \
        requests \
        nvidia-ml-py3 \
        tensorflow-gpu \
        && \
    python3 -m ipykernel.kernelspec

RUN pip install --upgrade pip && \
    pip install \
        Pillow \
        h5py \
        ipykernel \
        matplotlib \
        numpy \
        pandas \
        scipy \
        sklearn \
        scikit-image \
        opencv-python\
        tqdm \
        requests \
        nvidia-ml-py \
        tensorflow-gpu \
        tensorflow-serving-api \
        && \
    python -m ipykernel.kernelspec && \
    rm -rf ~/.cache/pip

# Set up our notebook config.
COPY jupyter_notebook_config.py /root/.jupyter/

# Jupyter has issues with being run directly:
#   https://github.com/ipython/ipython/issues/7062
# We just add a little wrapper script.
COPY run_jupyter.sh /

# For CUDA profiling, TensorFlow requires CUPTI.
ENV LD_LIBRARY_PATH /usr/local/cuda/extras/CUPTI/lib64:$LD_LIBRARY_PATH

# TensorBoard
EXPOSE 6006

# IPython
EXPOSE 8888

VOLUME ["/content"]

# Set work directory
WORKDIR "/content"

# Run Jupyter notebook
CMD ["/run_jupyter.sh", "--allow-root"]