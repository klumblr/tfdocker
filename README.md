# Modified tensorflow docker

## Requires
- cuda
- docker
- nvidia-docker

<br>
## How to build
```shell
$ docker build -t tf_docker .
```

<br>
## Create container
```shell
$ nvidia-docker run -it -d \
    -p 8888:8888 \
    -p 6006:6006 \
    -v /mount/path:/content \
    -v /etc/localtime:/etc/localtime:ro \
    --restart always \
    --name tf_docker \
    klumblr/tf_docker
```

<br>
## Run container
```shell
$ docker start tf_docker
```

<br>
## jupyter notebook initial settings
- notebook dir : /content
- passwd : tfdocker

<br>
---