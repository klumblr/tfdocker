import os
from IPython.lib import passwd

c = c  # pylint:disable=undefined-variable
c.NotebookApp.ip = '*'
c.NotebookApp.port = int(os.getenv('PORT', 8888))
c.NotebookApp.open_browser = False

# password : tfdocker
c.NotebookApp.password = 'sha1:69770a5e1740:4642e94ec949dceb9b6602719fc0f10e95cda48b'
c.NotebookApp.notebook_dir = '/content'